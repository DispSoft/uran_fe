import React from "react";
import TopNav from "./components/TopNav"
const logo = require('./assets/logo.svg');

function App() {
const Users = [
{
  id: "01",
  name: "Leanne Graham",
  email: "Sincere@april.biz",
  zipcode: 12112
},
{
  id: "02",
  name: "Ervin Howell",
  email: "Shanna@melissa.tv",
  zipcode: 12111
}
];

return (
  <div>
  <TopNav />
  <ul>
  {Users.map((data) => (
    <li key={data.id}>
   <p>{data.name}</p>
    <p>{data.email}</p>
    <p>{data.zipcode}</p>
    </li>
  ))}
  </ul>
  <img src={logo} className="App-logo" alt="logo" />
            <object
                type="image/svg+xml"
                data={String(logo)}
                width="64"
                height="64"
            >SVG</object>
  </div>
);
}

export default AppMy;