import TopNav from "./components/TopNav"
import MainFrame from "./components/MainFrame"
import './App.css';

function App () {
    return (
        <div className="App" id="app_container">
            <TopNav />
            <MainFrame id = "main_frame" />
        </div>
    );

   }

   export default App;