import '../../App.css';
import logo from '../../assets/chip.svg';

function LogoLoad() {
  return (
            <img
                src={logo}
                className="App-logo"
                width = "24"
                height = "24"
              />
  );
}

export default LogoLoad;