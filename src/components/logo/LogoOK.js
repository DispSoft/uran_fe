import '../../App.css';
import logo from '../../assets/chip_g.svg';

function LogoOK() {
  return (
            <img
                src={logo}
                //className="App-logo"
                alt="logo"
                width = "24"
                height = "24"
              />
  );
}

export default LogoOK;