const logo = require('../assets/clogow.svg');


function NavBrand() {
  return (
              <img
                src={logo.default}
                //className="App-logo"
                alt="logo"
                width = "48"
                height = "48"
              />
  );
}

export default NavBrand;