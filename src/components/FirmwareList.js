import React, { Component } from 'react'
import axios from 'axios'
import Table from 'react-bootstrap/Table';
import BlockIcon from '@mui/icons-material/Block';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';

class FirmwareList extends Component {
    constructor(props){
        super(props)
        this.state = {
          fw: []
        }
      }

      componentDidMount(){
        axios.get("https://upd.plfn.org:3000/api/firmwares")
          .then(res => {
            const fw = res.data;
            console.log(res)
            this.setState({ fw });
          })
      }

      render() {
         return (
         <>
            <h3>
                Firmware List
            </h3>
            <Table striped bordered variant="dark" size = "sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Firmware</th>
                  <th>Path</th>
                  <th>Offset</th>
                  <th>Disable firmware</th>
                </tr>
              </thead>
              <tbody>
                {this.state.fw.map(d => (
                    <tr>
                      <td>{d.id}</td>
                      <td>{d.name}</td>
                      <td>{d.offset}</td>
                      <td>{d.path}</td>
                      <td><a href="#home"><BlockIcon style={{ color: 'red' }} /></a></td>
                    </tr>
                ))}
              </tbody>
            </Table>
         </>
        )
    }
};

export default FirmwareList;