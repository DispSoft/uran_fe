// import AccountCircleIcon from '@mui/icons-material/AccountCircle';
// import HomeIcon from '@mui/icons-material/Home';
import DeviceList from "./DeviceList"
import FirmwareList from "./FirmwareList"

function MainFrame () {
    return (

            <div className="App-header">
                <div style={{ marginTop: "12vh" }}>

                <DeviceList />
                <FirmwareList />

                </div>
            </div>

    );

   }

   export default MainFrame;