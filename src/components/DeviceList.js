import React, { Component } from 'react'
import axios from 'axios'
import Table from 'react-bootstrap/Table';
import LogoOK from './logo/LogoOK'
import LogoLoad from './logo/LogoLoad'
import Moment from 'react-moment';


Moment.globalFormat = 'DD.MM.YYYY hh:mm';

class DeviceList extends Component {
    constructor(props){
        super(props)
        this.state = {
          fw: []
        }
      }

      componentDidMount(){
        axios.get("https://upd.plfn.org:3000/api/devices")
          .then(res => {
            const fw = res.data;
            console.log(res)
            this.setState({ fw });
          })
      }

      get_date(cur_date, start_date){
        var sec = (cur_date-start_date)
        if(sec < 60) return sec + " sec"
        if(sec < 60*60) return Math.floor(sec/60) + " min " + Math.floor(sec % 60) + " sec"
        return "> 1 hour"
      }

      render() {
         return (
         <>
            <h3>
                Device List
            </h3>
            <Table striped bordered variant="dark" size = "sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Device</th>
                  <th>FW</th>
                  <td>Current offset</td>
                  <th>Start time</th>
                  <th>Last time</th>
                  <th>Finish time</th>
                  <th>Flash time</th>
                  <th>Last request</th>
                  <th>Channel</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {this.state.fw.map(d => (
                    <tr>
                      <td>{d.id}</td>
                      <td>{d.mac}</td>
                      <td>{d.fw_name}</td>
                      <td>{d.offset}</td>
                      <td><Moment unix>{d.start_time}</Moment></td>
                      <td><Moment unix>{d.last_time}</Moment></td>
                      <td><Moment unix>{d.end_time}</Moment></td>
                      <td>{this.get_date(d.last_time, d.start_time)}</td>
                      <td>
                         {this.get_date(Date.now(), d.last_time)} ago
                      </td>
                      <td>{d.channel}</td>
                      <td><td>
                        {d.is_done > 0? <LogoOK /> : <LogoLoad />}
                        {d.downloaded}%</td></td>
                      <td>
                        <a href="https://lk.plfn.org/object/4623/obj-event">
                        Go to LK
                        </a>
                      </td>

                    </tr>
                ))}
              </tbody>
            </Table>
         </>
        )
    }
};

export default DeviceList;