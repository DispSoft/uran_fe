import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavBrand from "./NavBrand"
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import HomeIcon from '@mui/icons-material/Home';
import MemoryIcon from '@mui/icons-material/Memory';
import CloudDownloadIcon from '@mui/icons-material/CloudDownload';

function TopNav() {
  return (
    <Navbar bg="dark" expand="lg"  variant="dark" height="50px" fixed="top">

      <Container>
        <Navbar.Brand href="#home">
        <NavBrand />  {'   '}
        PLFN Firmware Monitor
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" align="right">
          <Nav className="me-auto">
            <Nav.Link href="#home"><HomeIcon /> Home</Nav.Link>
            <Nav.Link href="https://lk.plfn.org"><AccountCircleIcon /> LK</Nav.Link>
            <Nav.Link href="https://lk.plfn.org"><MemoryIcon /> Firmware list</Nav.Link>
            <Nav.Link href="https://lk.plfn.org"><CloudDownloadIcon /> Devices</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default TopNav;